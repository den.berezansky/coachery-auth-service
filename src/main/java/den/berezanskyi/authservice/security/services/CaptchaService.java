package den.berezanskyi.authservice.security.services;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;


/**
 * Created by Denis Berezanskiy on 26.03.2020.
 */

@Service
public class CaptchaService {
    
    private final String GOOGLE_URL = "https://www.google.com/recaptcha/api/siteverify";
    @Value("${captcha.secret.key}")
    private String secretKey;
    
    public boolean isVerified(String key) {
        String USER_AGENT = "Mozilla/5.0";
        if (key == null || "".equals(key)) {
            return false;
        }
        try {
            URL obj = new URL(GOOGLE_URL);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            // add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            String postParams = "secret=" + secretKey + "&response=" + key;
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // parse JSON response and return 'success' value
            JsonObject jsonObject = new Gson().fromJson(response.toString(), JsonObject.class);
            
            return  jsonObject.get("success") != null ? jsonObject.get("success").getAsBoolean() : false;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

