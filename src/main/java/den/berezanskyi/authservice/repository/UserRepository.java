package den.berezanskyi.authservice.repository;

import java.util.Optional;

import den.berezanskyi.authservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Denis Berezanskiy on 12.03.2020.
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
    
    Boolean existsByPhoneNumber(String phoneNumber);
    
    Boolean existsByEmail(String email);
}
