package den.berezanskyi.authservice.payloads.request;

/**
 * Created by Denis Berezanskiy on 26.03.2020.
 */
public class CaptchaRequest {
    private String googleCaptchaResponse;
    
    public String getGoogleCaptchaResponse() {
        return googleCaptchaResponse;
    }
    
    public void setGoogleCaptchaResponse(String googleCaptchaResponse) {
        this.googleCaptchaResponse = googleCaptchaResponse;
    }
}
