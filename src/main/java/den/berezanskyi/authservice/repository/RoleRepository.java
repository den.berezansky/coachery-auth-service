package den.berezanskyi.authservice.repository;

import java.util.Optional;

import den.berezanskyi.authservice.model.ERole;
import den.berezanskyi.authservice.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Denis Berezanskiy on 12.03.2020.
 */

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
