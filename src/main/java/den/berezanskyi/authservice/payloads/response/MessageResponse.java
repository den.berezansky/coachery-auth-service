package den.berezanskyi.authservice.payloads.response;

/**
 * Created by Denis Berezanskiy on 12.03.2020.
 */

public class MessageResponse {
    private String message;
    
    public MessageResponse(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
}
