package den.berezanskyi.authservice.payloads.response;

/**
 * Created by Denis Berezanskiy on 26.03.2020.
 */
public class CaptchaResponse {
    private boolean captchaVerified;
    
    public CaptchaResponse(boolean captchaVerified) {
        this.captchaVerified = captchaVerified;
    }
    
    public boolean isCaptchaVerified() {
        return captchaVerified;
    }
    
    public void setCaptchaVerified(boolean captchaVerified) {
        this.captchaVerified = captchaVerified;
    }
}
