package den.berezanskyi.authservice.payloads.request;

import javax.validation.constraints.NotBlank;

/**
 * Created by Denis Berezanskiy on 12.03.2020.
 */

public class LoginRequest {
    @NotBlank
    private String email;
    
    @NotBlank
    private String password;
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
}