package den.berezanskyi.authservice.model;

/**
 * Created by Denis Berezanskiy on 12.03.2020.
 */
public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN,
    ROLE_COACH
}
