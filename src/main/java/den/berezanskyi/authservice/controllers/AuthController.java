package den.berezanskyi.authservice.controllers;

import den.berezanskyi.authservice.model.ERole;
import den.berezanskyi.authservice.model.Role;
import den.berezanskyi.authservice.model.User;
import den.berezanskyi.authservice.payloads.request.CaptchaRequest;
import den.berezanskyi.authservice.payloads.request.LoginRequest;
import den.berezanskyi.authservice.payloads.request.SignupRequest;
import den.berezanskyi.authservice.payloads.response.CaptchaResponse;
import den.berezanskyi.authservice.payloads.response.JwtResponse;
import den.berezanskyi.authservice.payloads.response.MessageResponse;
import den.berezanskyi.authservice.repository.RoleRepository;
import den.berezanskyi.authservice.repository.UserRepository;
import den.berezanskyi.authservice.security.jwt.JwtUtils;
import den.berezanskyi.authservice.security.services.CaptchaService;
import den.berezanskyi.authservice.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Denis Berezanskiy on 12.03.2020.
 */


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    RoleRepository roleRepository;
    
    @Autowired
    PasswordEncoder encoder;
    
    @Autowired
    JwtUtils jwtUtils;
    
    @Autowired
    CaptchaService captchaService;
    
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority()).collect(Collectors.toList());
        
        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getEmail(), roles));
    }
    
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByPhoneNumber(signUpRequest.getPhoneNumber())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Phone number is already taken!"));
        }
        
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
        }
        
        User user = new User(signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()),
                signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getPhoneNumber(), false);
        
        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();
        
        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
        else {
            strRoles.forEach(role -> {
                Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                Role coachRole = roleRepository.findByName(ERole.ROLE_COACH).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                Role userRole = roleRepository.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                switch (role) {
                    case "admin":
                        roles.add(adminRole);
                        roles.add(modRole);
                        roles.add(coachRole);
                        
                        break;
                    case "mod":
                        roles.add(modRole);
                        
                        break;
                    case "coach":
                        roles.add(coachRole);
                        
                        break;
                    default:
                        roles.add(userRole);
                }
            });
        }
        
        user.setRoles(roles);
        userRepository.save(user);
        
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
    
    @PostMapping("/captchaverify")
    public ResponseEntity<?> verifyCaptcha(@Valid @RequestBody CaptchaRequest captchaRequest) {
        if (captchaRequest.getGoogleCaptchaResponse().isEmpty()) {
            return ResponseEntity.badRequest().body("Error: Captcha response key must be not empty");
        }
        return ResponseEntity.ok(new CaptchaResponse(captchaService.isVerified(captchaRequest.getGoogleCaptchaResponse())));
    }
}
